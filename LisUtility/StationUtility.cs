﻿using System;
using Distribution;
using System.Collections.Generic;
using Diagramm;
//===================================================================
namespace LisUtility
{
   /// <summary>
   /// Содержит функции для вычисления данных, которые связаны с станциями
   /// </summary>
   public class StationUtility
   {
      #region Delegate
      /// <summary>
      /// Делегат для отображения отладочной информации.
      /// </summary>
      /// <param name="message">сообщение</param>
      public delegate void OutDebugMessage(string message);
      public static event OutDebugMessage DebugMessage;
      #endregion

      #region Public methods
      //===========================================================
      /// <summary>
      /// Возвращает коэффициент усиления антенны
      /// </summary>
      /// <param name="antDiam">диаметр антенны (м)</param>
      /// <param name="azimuth">азимут (град)</param>
      /// <param name="elevation">угол места (град)</param>
      /// <param name="freq">частота (МГц)</param>
      /// <returns>Коэффициент усиления антенны</returns>
      public static double AntennaGainWithAntDiam(double antDiam, double azimuth, double elevation, double freq)
      {
         double alpha = 300.0 / freq;
         SendDebugMessage("alpha = 300.0 / freq = " + alpha.ToString("G"));
         double maxGain = 20.0 * Math.Log10(antDiam / alpha) + 7.7;
         SendDebugMessage("maxGain = 20*log(D/alpha)+7.7 = " + maxGain.ToString("G"));
         return AntennaGain(antDiam, maxGain, azimuth, elevation, freq);
      }

      /// <summary>
      /// для знаходження найближчого числа з списку
      /// </summary>
      /// <param name="param">текстова строка за значеннями</param>
      /// <param name="delta">число до якого потрібно знайти ближайше по значенню</param>
      /// <returns></returns>
      public static double GFi(string param, double delta)
      {
         //if (param == "OMNI")
         //{
            AntennaDiagramm gfi = new AntennaDiagramm();
            gfi.Build(param);
            return gfi.GetLossesAmount(delta);
         //}
         //if (param == "")
         //{


         //}
         //AntennaDiagramm gfi1 = new AntennaDiagramm();
         //gfi1.Build(param);
         //return gfi1.GetLossesAmount(delta);

         //string[] data = param.Split(' ');
         //List<double> dataVal = new List<double>();
         //List<double> dataRealVal = new List<double>();

         //bool fl = true;
         //foreach (string str in data)
         //{
         //   double val = 0;
         //   try
         //   {
         //      val = Convert.ToDouble(str);
         //   }
         //   catch
         //   {
         //      System.Windows.Forms.MessageBox.Show(str);
         //   }
         //   if (fl)
         //      dataVal.Add(val);
         //   else
         //      dataRealVal.Add(val);
         //   fl = !fl;
         //}

         //if ((dataVal.Count > dataRealVal.Count) && (dataRealVal.Count > 0))
         //   dataRealVal.Add(dataRealVal[0]);

         //double raznica = 0;
         //double lastValue = 0;
         //double lastRaznica = 0; // остання різниця
         //int index = 0;

         //for (int i = 0; i < dataVal.Count; i++)
         //{
         //   double curVal = dataVal[i];

         //   if (curVal > delta)
         //   {
         //      lastRaznica = Math.Abs(delta - lastValue);
         //      raznica = curVal - delta;

         //      if (lastRaznica > raznica)
         //      {
         //         lastValue = curVal;
         //         break;
         //      }
         //      else
         //      {
         //         if (lastRaznica == raznica)
         //         {
         //            lastValue = curVal;
         //            continue;
         //         }
         //         else
         //            break;
         //      }
         //   }

         //   lastValue = curVal;
         //}

         //index = 0;
         //for (int i = 0; i < dataVal.Count; i++)
         //{
         //   double val = dataVal[i];
         //   if (val == lastValue)
         //   {
         //      index = i;
         //      break;
         //   }
         //}
         //if ((index >= dataRealVal.Count) && (dataRealVal.Count > 0))
         //   delta = dataRealVal[0]; // витягуємо реальне значення
         //else
         //   delta = dataRealVal[index]; // витягуємо реальне значення
         //return delta;
      }

      private static void CheckOnDigit(ref string diagram)
      {
         int index = 0;
         for (int i = 0; i < diagram.Length; i++)
         {
            if (char.IsDigit(diagram[i]))
            {
               index = i;
               break;
            }
         }
      }

      public static double AntennGainCalculation(double a1, double aZKartochki, double b1, double bZKartochki,
         double gMax, string aDiagH, string bDiagV, string antenDIAGA)
      {
         antenDIAGA = antenDIAGA.ToUpper();         
         double g = 0;        

         if (antenDIAGA.Equals("HV", StringComparison.OrdinalIgnoreCase))
         {
            double dAm = Math.Abs(a1 - aZKartochki);
            double dBm = Math.Abs(b1 - bZKartochki);           

            //int index = aDiagH.IndexOf(' ');
            //if (index > 0)
            //   aDiagH = aDiagH.Remove(0, index);

            //index = bDiagV.IndexOf(' ');
            //if (index > 0)
            //   bDiagV = bDiagV.Remove(0, index);

            double gta = gMax + GFi(aDiagH, dAm);
            double gtum = gMax + GFi(bDiagV, dBm);
            g = Math.Min(gta, gtum);
         }

         if (antenDIAGA.Equals("HH", StringComparison.OrdinalIgnoreCase))
         {
            double dAm = Math.Abs(a1 - aZKartochki);
            //int index = aDiagH.IndexOf(' ');
            //if (index > 0)
              // aDiagH = aDiagH.Remove(0, index + 1);
            double gta = gMax + GFi(aDiagH, dAm);
            g = gta;
         }

         if (antenDIAGA.Equals("MODE", StringComparison.OrdinalIgnoreCase))
         {
            g = gMax;
         }
    
         return g;
      }

      //===========================================================
      /// <summary>
      /// Возвращает коэффициент усиления антенны
      /// </summary>
      /// <param name="maxGain">максимальный коэффициент усиления антенны (дБ)</param>
      /// <param name="azimuth">азимут (град)</param>
      /// <param name="elevation">угол места (град)</param>
      /// <param name="freq">частота (МГц)</param>
      /// <returns>Коэффициент усиления антенны</returns>
      public static double AntennaGainWithMaxGain(double maxGain, double azimuth, double elevation, double freq)
      {
         double alpha = 300.0 / freq;
         SendDebugMessage("alpha = 300.0 / freq = " + alpha.ToString("G"));
         double antDiam = alpha * Math.Pow(10, (maxGain - 7.7) / 20.0);
         SendDebugMessage("D = alpha*Pow(10, (Gmax - 7.7) / 20.0) = " + antDiam.ToString("G"));
         return AntennaGain(antDiam, maxGain, azimuth, elevation, freq);
      }
      //===========================================================
      /// <summary>
      /// Возвращает коэффициент усиления антенны
      /// </summary>
      /// <param name="antDiam">диаметр антенны (м)</param>
      /// <param name="maxGain">максимальный коэффициент усиления антенны (дБ)</param>
      /// <param name="azimuth">азимут (град)</param>
      /// <param name="elevation">угол места (град)</param>
      /// <param name="freq">частота (МГц)</param>
      /// <returns>Коэффициент усиления антенны</returns>
      private static double AntennaGain(double antDiam, double maxGain, double azimuth, double elevation, double freq)
      {
         double retVal = 0.0;
         double phi = (azimuth > elevation) ? azimuth : elevation;  // phi = max(azimuth, elevation)
         if (phi < 0.0) phi = Math.Abs(phi);
         if (phi > 180.0) phi = Math.Abs(phi - 360);
         SendDebugMessage("phi = max(azimuth, elevation) = " + phi.ToString("G"));
         double alpha = 300.0 / freq;                               // alpha = 300 / f
         SendDebugMessage("alpha = 300.0 / freq = " + alpha.ToString("G"));
         double d_Div_aplha = antDiam / alpha;                      // D/alpha
         SendDebugMessage("D / alpha = " + d_Div_aplha.ToString("G"));
         double G1 = 2.0 + 15.0 * Math.Log10(antDiam / alpha);      // G1 = 2 + 15log(D/alpha)
         SendDebugMessage("G1 = 2 + 15log(D/alpha) = " + G1.ToString("G"));
         double phi_m = ((20.0 * alpha) / antDiam) * Math.Sqrt(maxGain - G1); // phi_m = (20*alpha) / D * Sqrt(Gmax - G1)
         SendDebugMessage("phi_m = (20*alpha) / D * Sqrt(Gmax - G1) = " + phi_m.ToString("G"));
         if (d_Div_aplha >= 100.0)
         {// Случай 1
            double phi_r = 15.85 * Math.Pow(antDiam / alpha, -0.6);
            SendDebugMessage("phi_r = 15.85 * Pow(D/alpha, -0.6) = " + phi_r.ToString("G"));
            if ((0.0 <= phi) && (phi < phi_m))
            {// 0 < phi < phi_m
               retVal = maxGain - 0.0025 * Math.Pow(antDiam / alpha * phi, 2.0);
            }
            else if ((phi_m <= phi) && (phi < phi_r))
            {// phi_m <= phi < phi_r
               retVal = G1;
            }
            else if ((phi_r <= phi) && (phi < 48.0))
            {// phi_r <= phi < 48
               retVal = 32.0 - 25.0 * Math.Log10(phi);
            }
            else //if ((48.0 <= phi) && (phi <= 180.0))
            {// 48 <= phi <= 180
               retVal = -10.0;
            }
         }
         else
         {// Случай 2
            double tmp = 100.0 * (alpha / antDiam);
            if ((0.0 <= phi) && (phi < phi_m))
            {// 0 < phi < phi_m
               retVal = maxGain - 0.0025 * Math.Pow((antDiam / alpha) * phi, 2);
            }
            else if ((phi_m <= phi) && (phi < tmp))
            {// phi_m <= phi < 100 * (alpha / D)
               retVal = G1;
            }
            else if ((tmp <= phi) && (phi < 48.0))
            {// 100 * (alpha / D) <= phi < 48
               retVal = 52.0 - 10 * Math.Log10(antDiam / alpha) - 25.0 * Math.Log10(phi);
            }
            else //if((48.0 <= phi) && (phi <= 180.0)
            {// 48 <= phi <= 180
               retVal = 10.0 - 10.0 * Math.Log10(antDiam / alpha);
            }
         }
         SendDebugMessage("result = " + retVal.ToString("G"));
         return retVal;
      }

      //===========================================================
      /// <summary>
      /// Возвращает коэффициент усиления антенны
      /// </summary>
      /// <param name="length1">долгота станции 1 [град]</param>
      /// <param name="width1">широта станции 1 [град]</param>        
      /// <param name="length2">долгота станции 2 [град]</param>
      /// <param name="width2">широта станции 2 [град]</param>        
      /// <returns>Расчет расстояния</returns>
      public static double AntennaLength(double length1, double width1,
          double length2, double width2)
      {
         double deltaWidth = width1 - width2;
         double deltaLength = length1 - length2;
         double deltaWidthMidle = (width1 + width2) / 2;
         double cosinus = Math.Cos(NSPosition.Position.DegreeToRadian(deltaWidthMidle));
         double d = Math.Sqrt(Math.Pow(111 * deltaWidth, 2) + Math.Pow(111 * deltaLength * cosinus, 2));
         return d;
      }

      //===========================================================
      /// <summary>
      /// Возвращает коэффициент усиления антенны
      /// </summary>
      /// <param name="length1">долгота станции 1 [град]</param>
      /// <param name="width1">широта станции 1 [град]</param>        
      /// <param name="length2">долгота станции 2 [град]</param>
      /// <param name="width2">широта станции 2 [град]</param> 
      /// <param name="a1">сылка на азимут 1 [град]</param>
      /// <param name="b2">сылка на азимут 2 [град]</param>
      /// <returns>Расчет расстояния</returns>
      public static void AntennaAzimuth(double length1, double width1,
          double length2, double width2, ref double a1, ref double a2)
      {
         double deltaWidth = width1 - width2;
         double deltaLength = (length1 - length2) * Math.Cos(Math.PI / 180 * (width1 / 2 + width2 / 2));
         double a = Math.Atan(Math.Abs(deltaWidth / deltaLength));
         a = NSPosition.Position.RadianToDegree(a);        

         if (length1 > length2 && width1 > width2)
         {
            a1 = 270 - a; a2 = 90 - a; return;
         }
         if (length1 < length2 && width1 > width2)
         {
            a1 = 90 + a; a2 = 270 + a; return;
         }
         if (length1 > length2 && width1 < width2)
         {
            a1 = 270 + a; a2 = 90 + a; return;
         }
         if (length1 < length2 && width1 < width2)
         {
            a1 = 90 - a; a2 = 270 - a; return;
         }
         if (length1 > length2 && width1 == width2)
         {
            a1 = 270; a2 = 90; return;
         }
         if (length1 < length2 && width1 == width2)
         {
            a1 = 90; a2 = 270; return;
         }
         if (length1 == length2 && width1 > width2)
         {
            a1 = 180; a2 = 0; return;
         }
         if (length1 == length2 && width1 < width2)
         {
            a1 = 0; a2 = 180; return;
         }
      }

      //===========================================================
      /// <summary>
      /// Возвращает коэффициент усиления антенны
      /// </summary>
      /// <param name="length1">долгота станции 1 [град]</param>
      /// <param name="width1">широта станции 1 [град]</param>        
      /// <param name="height1">высота станции над уровнем моря станции 1 [м]</param>
      /// <param name="length2">долгота станции 2 [град]</param>
      /// <param name="width2">широта станции 2 [град]</param>    
      /// <param name="height2">высота станции над уровнем моря станции 2 [м]</param>
      /// <param name="b1">сылка на угол места 1 [град]</param>
      /// <param name="b2">сылка на угол места 2 [град]</param>
      /// <returns>Расчет углов места</returns>
      public static void CornerPlace(double length1, double width1,
          double height1, double length2, double width2, double height2,
          ref double b1, ref double b2)
      {
         double d = AntennaLength(length1, width1, length2, width2);
         CornerPlace(d, height1, height2, ref b1, ref b2);
      }

      //===========================================================
      /// <summary>
      /// Возвращает коэффициент усиления антенны
      /// </summary>
      /// <param name="d">расстояние между станциями [км]</param>
      /// <param name="height1">высота станции над уровнем моря станции 1 [м]</param>        
      /// <param name="height2">высота станции над уровнем моря станции 2 [м]</param>
      /// <param name="b1">сылка на угол места 1 [град]</param>
      /// <param name="b2">сылка на угол места 2 [град]</param>
      /// <returns>Расчет углов места</returns>
      public static void CornerPlace(double d, double height1, double height2, ref double b1, ref double b2)
      {
         b1 = (((height2 - height1) / d) - ((1000 * d) / (2 * 8500))) * (180 / Math.PI * 0.001);
         b2 = (((height1 - height2) / d) - ((1000 * d) / (2 * 8500))) * (180 / Math.PI * 0.001);
      }

      //===========================================================
      /// <summary>
      /// Возвращает результат расчета мощности помех 
      /// </summary>
      /// <param name="pt">мощность передатчика[dBm]</param>        
      /// <param name="gT">коэффициент усиления антенны передатчика в направлении приемника[dBm]</param>
      /// <param name="lfT">ослабление в фидере передатчика [dBm]</param>        
      /// <param name="l525">ослабление на пролете распространения помехи[dBm]</param>
      /// <param name="gR">коэффициент усиления антенны приемника в направлении источника помех[dBm]</param>
      /// <param name="lfR">ослабление в фидере приемника[dBm]</param>                
      /// <param name="irf">защитный коэфициент[db]</param>            
      /// <returns>Расчет моощности помех</returns>  
      public static double PowerHindrance(double pt, double gT, double lfT, double l525,
          double gR, double lfR, double irf)
      {
         double pi = pt + gT - lfT - l525 + gR - lfR - irf;
         return pi;
      }

      //===========================================================
      /// <summary>
      /// Возвращает результат расчета TD
      /// </summary>
      /// <param name="pi">мощность помех[dBm]</param>                
      /// <param name="ktbf">уровень собственных шумов приемника</param>            
      /// <returns>Расчет TD</returns>
      public static double TD(double pi, double ktbf)
      {
         double td = 10 * Math.Log10(Math.Pow(10, pi / 10) + Math.Pow(10, ktbf / 10)) - ktbf;         
         return td;
      }

      #endregion
      //===========================================================
      //===========================================================
      //===========================================================
      #region Private methods

      private static void SendDebugMessage(string messge)
      {
         if (DebugMessage != null)
            DebugMessage(messge);
      }
      #endregion
   }
}
