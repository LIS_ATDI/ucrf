﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LisUtility.Mob
{
   public static class ACHMob
   {
      /// <summary>
      /// structure describing characteristics of the filter or mask
      /// </summary>
      public struct DataElement
      {
         public double frequency;
         public double losses;
      }

      /// <summary>
      /// compares frequency values of the filter or mask
      /// </summary>
      public struct DataElementComparer : IComparer<DataElement>
      {
         public int Compare(DataElement x, DataElement y)
         {
            if (x.frequency > y.frequency)
               return 1;
            else if (x.frequency == y.frequency)
            {
               return 0;
            }
            else
            {
               return -1;
            }
         }
      }

      private static void AddFrequency(double maskaTX, double maskaRX, double filterTX,
         double filterRX, List<DataElement> maskaT, List<DataElement> maskaR,
         List<DataElement> filterT, List<DataElement> filterR)
      {
         for (int i = 0; i < maskaT.Count; i++)
         {
            DataElement element = maskaT[i];
            element.frequency += maskaTX;
            maskaT[i] = element;
         }
         for (int i = 0; i < maskaR.Count; i++)
         {
            DataElement element = maskaR[i];
            element.frequency += maskaRX;
            maskaR[i] = element;
         }
         for (int i = 0; i < filterT.Count; i++)
         {
            DataElement element = filterT[i];
            element.frequency += filterTX;
            filterT[i] = element;
         }
         for (int i = 0; i < filterR.Count; i++)
         {
            DataElement element = filterR[i];
            element.frequency += filterRX;
            filterR[i] = element;
         }
      }

      private static void CompareLosses(List<DataElement> maskaOrFilter)
      {
         double prewFreq = -99999;        

         for (int i = 0; i < maskaOrFilter.Count; i++)
         {
            if (maskaOrFilter[i].frequency != prewFreq)
               prewFreq = maskaOrFilter[i].frequency;
            else
            {
               if (i + 1 < maskaOrFilter.Count)
               {
                  prewFreq = maskaOrFilter[i].frequency;

                  double currFreq = maskaOrFilter[i].frequency;
                  double nextFreq = maskaOrFilter[i + 1].frequency;
                  if (nextFreq > currFreq)
                     currFreq += 0.000001;
                  else
                     currFreq += -0.000001;

                  DataElement currElement = maskaOrFilter[i];
                  currElement.frequency = currFreq;
                  maskaOrFilter[i] = currElement;
               }
               else
               {
                  if (i - 2 > -1)
                  {
                     DataElement prewElement = maskaOrFilter[i - 2];
                     DataElement currElement = maskaOrFilter[i];
                     if (prewElement.frequency < maskaOrFilter[i].frequency)
                        currElement.frequency += 0.000001;
                     else
                        currElement.frequency += -0.000001;

                     maskaOrFilter[i] = currElement;
                  }                  
               }
            }
         }           
      }

      /// <summary>
      /// Fills empty MaskaTX or MaskaRX
      /// </summary>
      /// <param name="element">maskaTX or maskaRX</param>
      /// <param name="element">filterTX or filterRX</param>
      /// <param name="bw">ширина смуги частот передавача</param>
      /// <param name="filterTXorRX"></param>
      private static void FillEmptyMaska(List<DataElement> maska, List<DataElement> filter, double bw, double filterTXorRX, bool Intermodulation)
      {
         if (!Intermodulation)
         {
            double freq1 = filterTXorRX - bw / 2 - 0.000001;
            double freq2 = filterTXorRX - bw / 2;
            double freq3 = filterTXorRX + bw / 2;
            double freq4 = filterTXorRX + bw / 2 + 0.000001;
            DataElement firstElem = new DataElement { frequency = freq1, losses = 999 };
            DataElement seconElem = new DataElement { frequency = freq2, losses = 0 };
            DataElement thirdElem = new DataElement { frequency = freq3, losses = 0 };
            DataElement fourthElem = new DataElement { frequency = freq4, losses = 999 };
            maska.Add(firstElem);
            maska.Add(seconElem);
            maska.Add(thirdElem);
            maska.Add(fourthElem);
         }
         else
         {
            double minFreq = filter[0].frequency;
            double maxFreq = filter[filter.Count - 1].frequency;
            DataElement firstElem = new DataElement { frequency = minFreq, losses = 0 };
            DataElement seconElem = new DataElement { frequency = maxFreq, losses = 0 };
            maska.Add(firstElem);
            maska.Add(seconElem);
         }
      }

      private static void CheckOnValidateMaska(double filterTX, double filterRX, double bw,
         bool Intermodulation, List<DataElement> maskaT, List<DataElement> maskaR)
      {
         if (maskaT.Count == 0)
            FillEmptyMaska(maskaT, null, bw, filterTX, Intermodulation);

         if (!Intermodulation)
         {            
            if (maskaR.Count == 0)
               FillEmptyMaska(maskaR, null, bw, filterRX, Intermodulation);
         }
         else
         {           
            if (maskaR.Count == 0)
               FillEmptyMaska(maskaR, maskaT, 0, 0, Intermodulation);            
         }
      }

      /// <summary>
      /// returns a IRF value
      /// </summary>
      /// <param name="maskaF"></param>
      /// <param name="filterF"></param>
      /// <param name="maskaT">maska of transmitter</param>
      /// <param name="filterT">filter of transmitter</param>
      /// <param name="maskaR">maska of receiver</param>
      /// <param name="filterR">filter of receiver</param>
      /// <returns>IRF value</returns>
      public static double GetIRF(double maskaTX, double maskaRX, double filterTX,
         double filterRX, double bw, bool Intermodulation, List<DataElement> maskaT, List<DataElement> maskaR,
         List<DataElement> filterT, List<DataElement> filterR)
      {
         List<DataElement> _maskaT = new List<DataElement>(maskaT);
         List<DataElement> _maskaR = new List<DataElement>(maskaR);
         List<DataElement> _filterT = new List<DataElement>(filterT);
         List<DataElement> _filterR = new List<DataElement>(filterR);

         CheckOnValidateMaska(filterTX, filterRX, bw, Intermodulation, _maskaT,
            _maskaR);

         AddFrequency(maskaTX, maskaRX, filterTX, filterRX, _maskaT, _maskaR,
        _filterT, _filterR);

         List<DataElement> achT = null;
         List<DataElement> achR = null;
         bool dontCalcT = false;
         bool dontCalcR = false;

         CompareLosses(_maskaT);
         CompareLosses(_maskaR);
         CompareLosses(_filterT);
         CompareLosses(_filterR);


         if (_filterT.Count == 0)
         {
            achT = _maskaT;
            dontCalcT = true;
         }

         if (_filterR.Count == 0)
         {
            achR = _maskaR;
            dontCalcR = true;
         }         
         
         if (!dontCalcT)
            achT = CreateAchTable(_maskaT, _filterT);
         if (!dontCalcR)
            achR = CreateAchTable(_maskaR, _filterR);

         double znamenatel = GetConvolution(achT, achR);
         
         List<DataElement> tmp = new List<DataElement>();
         DataElement t = new DataElement() { frequency = _maskaT[0].frequency, losses = 0 };
         DataElement t1 = new DataElement() { frequency = _maskaT[_maskaT.Count - 1].frequency, losses = 0 };
         tmp.Add(t);
         tmp.Add(t1);

         double chislitel = GetConvolution(_maskaT, tmp);

         double irf = Math.Log10(chislitel / znamenatel) * 10;

         return irf;
      }

      public static double GetConvolution(List<DataElement> achT, List<DataElement> achR)
      {
         List<DataElement> achTi = CreateAchTableI(achT, achR);
         List<DataElement> achRi = CreateAchTableI(achR, achT);
                  
         int i2 = 0;
         double znamenik = 0;

         for (int i = 0; i < achTi.Count; i++)
         {
            if (i < achTi.Count - 1)
               i2 = i + 1;

            znamenik += (Math.Pow(10, -achRi[i2].losses / 10 - achTi[i2].losses / 10) +
               Math.Pow(10, -achRi[i].losses / 10 - achTi[i].losses / 10)) *
               (achTi[i2].frequency - achTi[i].frequency) / 2;
         }

         return znamenik; 
      }

      ///// <summary>
      ///// creates the ACH table
      ///// </summary>
      ///// <param name="fMaska"></param>
      ///// <param name="maska">elements of maska</param>
      ///// <param name="fFilter"></param>
      ///// <param name="filter">elements of filter)</param>
      ///// <returns>ACH table</returns>
      //private static List<DataElement> GetACHTable(double fMaska, List<DataElement> maska,
      //   double fFilter, List<DataElement> filter)
      //{
      //   for (int i = 0; i < maska.Count; i++)
      //   {
      //      DataElement element = maska[i];
      //      element.frequency += fMaska;
      //      maska[i] = element;
      //   }

      //   for (int i = 0; i < filter.Count; i++)
      //   {
      //      DataElement element = filter[i];
      //      element.frequency += fFilter;
      //      filter[i] = element;
      //   }

      //   List<DataElement> tmpAchTable = new List<DataElement>();
      //   List<DataElement> AchTable = new List<DataElement>();

      //   if (maska.Count > 0 && filter.Count > 0)
      //   {
      //      Check_A_Value(maska, filter);
      //      Check_A_Value(filter, maska);
      //   }

      //   //if (maska.Count > 0)
      //   foreach (DataElement f in maska)
      //   {
      //      tmpAchTable.Add(f);
      //   }

      //   //if (filter.Count > 0)
      //   foreach (DataElement f in filter)
      //   {
      //      tmpAchTable.Add(f);
      //   }

      //   DataElementComparer dc = new DataElementComparer();
      //   tmpAchTable.Sort(dc);

      //   if (maska.Count > 0 && filter.Count > 0)
      //   {
      //      bool flag = false;
      //      double lastLosses = 0;
      //      for (int i = 0; i < tmpAchTable.Count; i++)
      //      {
      //         if (flag)
      //         {
      //            DataElement tmp = tmpAchTable[i];
      //            tmp.losses += lastLosses;
      //            AchTable.Add(tmp);
      //            flag = !flag;
      //         }
      //         else
      //         {
      //            lastLosses = tmpAchTable[i].losses;
      //            flag = !flag;
      //         }
      //      }

      //      return AchTable;
      //   }

      //   return tmpAchTable;
      //}

      private static List<DataElement> CreateAchTable(List<DataElement> maskaList, List<DataElement> filterList)
      {
         List<DataElement> AchTable = new List<DataElement>();
         short mIterator = 0; // Iterator of mask
         short fIterator = 0; // Iterator of filter
         bool mEnd = false; // flag which shows us that the maska over
         bool fEnd = false; // flag which shows us that the filter over

         int maxCount = maskaList.Count + filterList.Count;

         for (int i = 0; i < maxCount; i++)
         {
            if (mIterator >= maskaList.Count)
               mEnd = true;
            if (fIterator >= filterList.Count)
               fEnd = true;

            double mFreq = -9999, fFreq = -9999;

            if (!mEnd)
               mFreq = maskaList[mIterator].frequency;
            else
            {
               // the masks iterator has max value

               if (fEnd)
                  continue;

               fFreq = filterList[fIterator].frequency;
               double flosses = filterList[fIterator].losses + maskaList[maskaList.Count - 1].losses;

               DataElement tmp = new DataElement() { frequency = fFreq, losses = flosses };
               AchTable.Add(tmp);
               fIterator++;
               continue;
            }

            if (!fEnd)
               fFreq = filterList[fIterator].frequency;
            else
            {
               // the filter iterator has max value   
               if (mEnd)
                  continue;

               mFreq = maskaList[mIterator].frequency;
               double mlosses = maskaList[mIterator].losses + filterList[filterList.Count - 1].losses;

               DataElement tmp = new DataElement() { frequency = mFreq, losses = mlosses };
               AchTable.Add(tmp);
               mIterator++;
               continue;
            }

            if (mFreq == fFreq)
            {
               double sumLosses = maskaList[mIterator].losses + filterList[fIterator].losses;
               DataElement tmp = new DataElement() { frequency = mFreq, losses = sumLosses };
               AchTable.Add(tmp);
               mIterator++;
               fIterator++;
               i++;
               continue;
            }

            if (mFreq < fFreq) // freq of maska is more then freq of filter 
            {
               int lastIndex = fIterator - 1;// >= 0 ? fIterator - 1 : 0;               
               double filterLosses;

               if (lastIndex > -1)
               {
                  double prewFreq = filterList[lastIndex].frequency;
                  double prewLoss = filterList[lastIndex].losses;

                  double losses = filterList[fIterator].losses;

                  filterLosses = prewLoss + (mFreq - prewFreq) *
                     ((losses - prewLoss) / (fFreq - prewFreq));
               }
               else
               {
                  filterLosses = filterList[fIterator].losses;
               }

               filterLosses += maskaList[mIterator].losses;

               DataElement tmp = new DataElement() { frequency = mFreq, losses = filterLosses };
               AchTable.Add(tmp);
               mIterator++;
               continue;
            }
            else // freq of filter is more then freq of maska
            {
               int lastIndex = mIterator - 1;//= mIterator - 1 >= 0 ? mIterator - 1 : 0;             
               double maskaLosses;

               if (lastIndex > -1)//lastIndex >= 0 && mIterator - 1 > -1)
               {
                  double prewFreq = maskaList[lastIndex].frequency;
                  double prewLoss = maskaList[lastIndex].losses;

                  double losses = maskaList[mIterator].losses;

                  maskaLosses = prewLoss + (fFreq - prewFreq) *
                  ((losses - prewLoss) / (mFreq - prewFreq));
               }
               else
               {
                  maskaLosses = maskaList[mIterator].losses;
               }

               maskaLosses += filterList[fIterator].losses;

               DataElement tmp = new DataElement() { frequency = fFreq, losses = maskaLosses };
               AchTable.Add(tmp);
               fIterator++;
               continue;
            }
         }

          return AchTable;
      }

      private static List<DataElement> CreateAchTableI(List<DataElement> maskaList, List<DataElement> filterList)
      {
         List<DataElement> AchTablei = new List<DataElement>();

         if (maskaList.Count == 0)
         {
            AchTablei.Add(new DataElement() { frequency = 0.0, losses = 0.0 });
            AchTablei.Add(new DataElement() { frequency = 100000.0, losses = 0.0 });
            return AchTablei;
         }

         short mIterator = 0; // Iterator of mask
         short fIterator = 0; // Iterator of filter
         bool mEnd = false; // flag which shows us that the maska over
         bool fEnd = false; // flag which shows us that the filter over

         int maxCount = maskaList.Count + filterList.Count;

         for (int i = 0; i < maxCount; i++)
         {
            if (mIterator >= maskaList.Count)
               mEnd = true;
            if (fIterator >= filterList.Count)
               fEnd = true;

            double mFreq = -9999, fFreq = -9999;

            if (!mEnd)
               mFreq = maskaList[mIterator].frequency;
            else
            {
               // the masks iterator has max value

               if (fEnd)
                  continue;

               fFreq = filterList[fIterator].frequency;
               double flosses = maskaList[maskaList.Count - 1].losses;

               DataElement tmp = new DataElement() { frequency = fFreq, losses = flosses };
               AchTablei.Add(tmp);
               fIterator++;
               continue;
            }

            if (!fEnd)
               fFreq = filterList[fIterator].frequency;
            else
            {
               // the filter iterator has max value   
               if (mEnd)
                  continue;

               mFreq = maskaList[mIterator].frequency;
               double mlosses = maskaList[mIterator].losses;

               DataElement tmp = new DataElement() { frequency = mFreq, losses = mlosses };
               AchTablei.Add(tmp);
               mIterator++;
               continue;
            }

            if (mFreq == fFreq)
            {
               double sumLosses = maskaList[mIterator].losses;
               DataElement tmp = new DataElement() { frequency = mFreq, losses = sumLosses };
               AchTablei.Add(tmp);
               mIterator++;
               fIterator++;
               i++;
               continue;
            }

            if (mFreq < fFreq) // freq of maska is more then freq of filter 
            {
               int lastIndex = fIterator - 1;// >= 0 ? fIterator - 1 : 0;               
               //double filterLosses;

               //if (lastIndex > -1)
               //{
               //   double prewFreq = filterList[lastIndex].frequency;
               //   double prewLoss = filterList[lastIndex].losses;

               //   double losses = 0;

               //   filterLosses = 0;
               //}
               //else
               //{
               //   filterLosses = 0;
               //}

               double filterLosses = maskaList[mIterator].losses;

               DataElement tmp = new DataElement() { frequency = mFreq, losses = filterLosses };
               AchTablei.Add(tmp);
               mIterator++;
               continue;
            }
            else // freq of filter is more then freq of maska
            {
               int lastIndex = mIterator - 1;//= mIterator - 1 >= 0 ? mIterator - 1 : 0;             
               double maskaLosses;

               if (lastIndex > -1)//lastIndex >= 0 && mIterator - 1 > -1)
               {
                  double prewFreq = maskaList[lastIndex].frequency;
                  double prewLoss = maskaList[lastIndex].losses;

                  double losses = maskaList[mIterator].losses;

                  maskaLosses = prewLoss + (fFreq - prewFreq) *
                  ((losses - prewLoss) / (mFreq - prewFreq));
               }
               else
               {
                  maskaLosses = maskaList[mIterator].losses;
               }

               DataElement tmp = new DataElement() { frequency = fFreq, losses = maskaLosses };
               AchTablei.Add(tmp);
               fIterator++;
               continue;
            }
         }

         return AchTablei;
      }

      ///// <summary>
      ///// interpolation for two elements lists
      ///// </summary>
      ///// <param name="maskaList">any elements (maybe maska or filter)</param>
      ///// <param name="filterList">any elements (maybe maska or filter)</param>
      //private static void Check_A_Value(List<DataElement> maskaList, List<DataElement> filterList)
      //{
      //   foreach (DataElement maska in maskaList)
      //   {
      //      bool i = false;
      //      foreach (DataElement filter in filterList)
      //      {
      //         if (filter.frequency == maska.frequency)
      //         {
      //            i = true;
      //            break;
      //         }
      //      }

      //      if (!i)
      //      {
      //         double minFreq = 0; double maxFreq = 0;
      //         double minA = 0; double maxA = 0;

      //         foreach (DataElement filter in filterList)
      //         {
      //            if (filter.frequency > maska.frequency)
      //            {
      //               maxFreq = filter.frequency;
      //               maxA = filter.losses;
      //               break;
      //            }
      //            minFreq = filter.frequency;
      //            minA = filter.losses;
      //         }

      //         double a = minA - (maska.frequency - minFreq) * (maxA - minA) / (maxFreq - minFreq);
      //         DataElement newfilte = new DataElement();
      //         newfilte.frequency = maska.frequency;
      //         newfilte.losses = a;
      //         filterList.Add(newfilte);
      //      }
      //   }
      //}
   }
}
