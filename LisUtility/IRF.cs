﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NSIRF
{
   public class IRF
   {
      public enum System
      {
         Digital,
         Analog
      }

      // отримую значення IRF
      public static double GetIRF(double FT, double FR, double deltaFt,
          double deltaFr, System sys)
      {
         if (deltaFt <= deltaFr)
         {
            double DF = GetDF(FT, FR, deltaFt, deltaFr);
            double irfZvezd = IRFsZvezda(DF, sys);
            return irfZvezd;
         }
         else
         {
            double DF = GetDF(FT, FR, deltaFt, deltaFr);
            double irfZvezd = IRFsZvezda(DF, sys);
            double irf = irfZvezd + 10 * Math.Log10(deltaFt / deltaFr);
            return irf;
         }
      }

      // отримую значення DF
      public static double GetDF(double FT, double FR, double deltaFt,
          double deltaFr)
      {
         double bn = Math.Max(deltaFr, deltaFt);
         double df = Math.Abs(FT - FR) / bn;
         return df;
      }

      // отримую IRF* з зірочкою
      public static double IRFsZvezda(double DF, System sys)
      {
         if (sys == System.Digital)
         {
            if (DF >= 0 && DF <= 0.5)
            {
               return 0.0;
            }

            if (DF > 0.5 && DF <= 1)
            {
               double raznica = 1 - DF; // різниця між максимальною точкою і DF
               double scalaValue = 0.5 - raznica;
               double procent = scalaValue / 0.5;
               double result = 25.0 * procent;
               return result;
            }
            if (DF > 1 && DF <= 2)
            {
               double raznica = 2 - DF; // різниця між максимальною точкою і DF
               double scalaValue = 1 - raznica;
               //double procent = scalaValue;// / 1;
               double result = 25.0 + 20.0 * scalaValue;//procent;
               return result;
            }
            if (DF > 2 && DF <= 3)
            {
               double raznica = 3 - DF; // різниця між максимальною точкою і DF
               double scalaValue = 1 - raznica;
               //double procent = scalaValue;// / 1;
               double result = 45.0 + 15.0 * scalaValue;//procent;
               return result;
            }
            if (DF > 3 && DF < 4)
            {
               return 60.0;
            }

            return 600.0;
         }
         else
         {
            if (DF >= 0 && DF <= 0.5)
            {
               return 0.0;
            }

            if (DF > 0.5 && DF <= 1.075)
            {
               double raznica = 1.075 - DF; // різниця між максимальною точкою і DF
               double scalaValue = 0.575 - raznica;
               double procent = scalaValue / 0.575;// * 100;
               double result = 60 * procent;/// 100 * procent;
               return result;
            }
            if (DF > 1.075 && DF < 5)
            {
               return 60.0;
            }

            return 600.0;
         }
      }
   }
}
